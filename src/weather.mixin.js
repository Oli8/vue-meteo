export default {
  methods: {
    temperature(temp) {
      return Math.round(temp) + '°';
    },
  },
}
